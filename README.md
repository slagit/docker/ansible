# Ansible

This container contains the ansible configuration management tool.

By default, this container will run as the non-root ansible user.
